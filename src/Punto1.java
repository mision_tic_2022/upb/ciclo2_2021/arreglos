/****************
 * Autor: Luis Enrique Suarez
 * Año: 2021
 * Ciclo 2 - MINTIC- UPB
 ***************/

import java.util.Scanner;

public class Punto1
{
    public static void main(String[] args)
    {
        int i;
        int[] numeros = new int[5];

        Scanner teclado = new Scanner(System.in);

        for(i=0; i<numeros.length; i++)
        {
            System.out.printf("Introduzca número %d: ", i+1);
            numeros[i] = teclado.nextInt();
        }

        for(i=0; i<numeros.length; i++)
        {
            System.out.println(numeros[i]);
        }
    }
}