import java.util.Scanner;

public class Calculadora {
    public static void main(String[] args) {
        calculadora();
    }

    public static void calculadora() {

        // Pido los datos de la operación y los números para calcular
        try (Scanner entrada = new Scanner(System.in)) {
            System.out.println(
                    "Escriba qué operación desea realizar: \n-Sumar \n-Restar \n-Multiplicar \n-Dividir \n-Elevar");
            String operacion = entrada.next();
            System.out.print("Ingrese el primer número: ");
            int num1 = entrada.nextInt();
            System.out.print("Ingrese el segundo número: ");
            int num2 = entrada.nextInt();

            // Realizo el switch para calcular según la operación escogida
            Double resultado = null;
            switch (operacion) {
                case "Sumar":
                    resultado = sumar(num1, num2);
                    break;
                case "Restar":
                    resultado = restar(num1, num2);
                    break;
                case "Multiplicar":
                    resultado = multiplicar(num1, num2);
                    break;
                case "Dividir":
                    resultado = dividir(num1, num2);
                    break;
                case "Elevar":
                    resultado = elevar_potencia(num1, num2);
                    break;
                default:
                    break;
            }
            System.out.println("El resultado es: "+resultado);
            /*
              * No me permite poner el "System.out.println("El resultado es: "+resultado);"
              * fuera del switch Tuve que ponerlo 5 veces dentro de cada case
              */
        } catch (Exception e) {
            System.err.println("Error en la entrada de datos.");
        }
    }

    // =======Los métodos para los cálculos matemáticos====
    public static double sumar(double num1, double num2) {
        double sumar = num1 + num2;
        return sumar;
    }

    public static double restar(double num1, double num2) {
        double restar = num1 - num2;
        return restar;
    }

    public static double multiplicar(double num1, double num2) {
        double multiplicar = num1 * num2;
        return multiplicar;
    }

    public static double dividir(double num1, double num2) {
        double dividir = num1 / num2;
        return dividir;
    }

    public static double elevar_potencia(double num1, double num2) {
        double elevar_potencia = Math.pow(num1, num2);
        return elevar_potencia;
    }
}
