/****************
 * Autor: Luis Enrique Suarez
 * Año: 2021
 * Ciclo 2 - MINTIC- UPB
 ***************/

import java.security.SecureRandom;
import java.util.Scanner;

public class Punto3 {
    public static void main(String[] args) {
        dato_inicial();

    }

    public static void dato_inicial() {
        // Declarar el arreglo
        int[] dato;
        try (Scanner entrada = new Scanner(System.in)) {
            System.out.println("Por favor ingrese el tamaño del arreglo: ");
            int entradas = entrada.nextInt();
            dato = new int[entradas];
            // Llenar el arreglo
            SecureRandom random = new SecureRandom();
            for (int i = 0; i < dato.length; i++) {
                dato[i] = random.nextInt(10) + 6;
            }
            mostrar(dato);

        } catch (Exception e) {
            System.err.println("Error");
        }

    }

    public static void mostrar(int[] datos) {
        for (int i = 0; i < datos.length; i++) {
            System.out.println(i + ". " + datos[i]);
        }
    }
}
