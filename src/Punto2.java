import java.util.Scanner;

public class Punto2 {
    public static void main(String[] args) {
        crear_llenar_arreglo();
    }

    public static void crear_llenar_arreglo() {
        // Declarar el arreglo
        int[] numeros;
        try (Scanner entrada = new Scanner(System.in)) {
            //Solicitamos por consola el tamaño al usuario
            System.out.println("Por favor ingrese el tamaño del arreglo; ");
            int tamanio = entrada.nextInt();
            //Creamos el arreglo con el tamaño ingresado
            numeros = new int[tamanio];
            //Llenar el arreglo
            for(int i = 0; i < numeros.length; i++){
                //Solicitamos el valor por teclado y lo almacenamos
                //directamente al arreglo
                System.out.println("Por favor el valor "+i+": ");
                numeros[i] = entrada.nextInt();
            }
            mostrar_datos(numeros);
        } catch (Exception e) {
            // TODO: handle exception
            System.err.println("Error");
        }

        
    }


    public static void mostrar_datos(int[] numeros){
        for(int i = 0; i < numeros.length; i++){
            //System.out.println("numeros["+i+"]: "+numeros[i]);
            System.out.printf("numeros[%d]: %d", i, numeros[i]);
            System.out.println("");
        }
    }
}
