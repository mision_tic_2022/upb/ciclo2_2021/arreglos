/**************
 * Autor: Edgar Ospina
 * Año 2021
 * MINTIC - UPB - CICLO 2
 */
import java.security.SecureRandom;

public class Punto4 {
    public static void main(String[] args) throws Exception {
        tirar_dado();
    }

    public static void tirar_dado() {

        SecureRandom random = new SecureRandom();
        int array[][] = new int[4][5];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = 1 + random.nextInt(6);
            }
        }

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + "\t");
            }
            System.out.println("");
        }
    }
}